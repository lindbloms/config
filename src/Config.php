<?php

namespace Lnd\Config;

require realpath('.').'/vendor/autoload.php';

use Symfony\Component\Yaml\Yaml;

class Config {
    private $config;

    function __construct(string $name = null, string $dirPath = null) {
       $path = $this->findDir($dirPath);
       $name = $this->findName($path, $name);

       $this->config = Yaml::parse(file_get_contents($path.DIRECTORY_SEPARATOR.$name));
    }

    function get(string $property): array {
        return isset($this->config[$property]) ? $this->config[$property] : null;
    }

    function findName(string $path, string $name = null): string {
        if ($name != null) {
            $files = glob($path.DIRECTORY_SEPARATOR."$name.{yml,yaml}", GLOB_BRACE);

            if (count($files) > 1) {
                throw new \Exception('Found multiple files with name: '.$name);
            } elseif (count($files) < 1) {
                throw new \Exception('Cannot find file with name: '.$name);
            }
        } else {
            $files = glob($path.DIRECTORY_SEPARATOR."*.{yml,yaml}", GLOB_BRACE);

            if (count($files) != 1) {
                throw new \Exception('Cannot determine which file to process');
            }
        }

        return basename($files[0]);
    }

    function findDir(string $path = null): string {
        if ($path != null) {
            if (is_dir($path)) {
                return $path;
            } else {
                throw new \InvalidArgumentException('Cannot find path: '.$path);
            }
        }

        $conventionPath = realpath('./config');

        if (!$conventionPath) {
            throw new \Exception('Cannot find config directory');
        }

        return $conventionPath;
    }
}

?>
